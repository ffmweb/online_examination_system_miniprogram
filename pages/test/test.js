// pages/test/test.js

const api = require("../../utils/api.js");
const util = require("../../utils/util.js");
const app = getApp();

var zuodaInfo = [];
var mulZuoDaInfo = [];
var zhuguanZuoDaInfo = [];

Page({
  /**
   * 页面的初始数据
   */
  data: {
    index: 0,
    questionArrays: [],
    questionInfo: {},
    optionInfo: {},

    selectOption: "",
    mulSelectOption: "",
    textContext: "",

    radioA: false,
    radioB: false,
    radioC: false,
    radioD: false,

    checkedboxA: false,
    checkedboxB: false,
    checkedboxC: false,
    checkedboxD: false,

    time: ""
  },

  swichRadio: function(e) {
    if (e.currentTarget.id === "A") {
      this.setData({
        radioA: !this.data.radioA
      });
      if (!this.data.radioA) {
        this.deleteZuoDa();
      }
    } else if (e.currentTarget.id === "B") {
      this.setData({
        radioB: !this.data.radioB
      });
      if (!this.data.radioB) {
        this.deleteZuoDa();
      }
    } else if (e.currentTarget.id === "C") {
      this.setData({
        radioC: !this.data.radioC
      });
      if (!this.data.radioC) {
        this.deleteZuoDa();
      }
    } else {
      this.setData({
        radioD: !this.data.radioD
      });
      if (!this.data.radioD) {
        this.deleteZuoDa();
      }
    }
  },

  /**
   * 从答题记录数组中删除该答题记录
   */
  deleteZuoDa: function() {
    for (let i = 0; i <= zuodaInfo.length - 1; i++) {
      if (
        this.data.questionArrays[this.data.index].que_no === zuodaInfo[i].que_no
      ) {
        zuodaInfo.splice(i, 1);
      }
    }
  },

  /**
   * 答题记录添加到答题记录数组中
   */
  addZuoDa: function() {
    let zuoda = {
      que_no: this.data.questionArrays[this.data.index].que_no,
      question: this.data.questionArrays[this.data.index].question,
      goodkey: this.data.questionArrays[this.data.index].goodkey,
      selectOption: this.data.selectOption
    };

    if (zuodaInfo.length === 0) {
      zuodaInfo.push(zuoda);
    } else {
      let noInsert = true;
      for (let i = 0; i < zuodaInfo.length; i++) {
        //找到已经答过的题，加以修改
        if (zuoda.que_no === zuodaInfo[i].que_no) {
          zuodaInfo[i] = zuoda;
          noInsert = false;
          break;
        }
      }
      //没有找到到已经答过的题，添加到答题数组中
      if (noInsert) {
        zuodaInfo.push(zuoda);
      }
    }
  },

  radioChange: function(e) {
    this.setData({
      selectOption: e.detail.value
    });
    this.addZuoDa();
  },

  /**
   * 变更题目
   */
  changetQuestion: function(event) {
    //渲染下一题题目以及选项
    this.setData({
      index: event.currentTarget.dataset.index
    });
    this.setData({
      questionInfo: this.data.questionArrays[this.data.index]
    });

    if (!(this.data.questionArrays[this.data.index].type == "主观题")) {
      this.setData({
        optionInfo: this.data.questionArrays[this.data.index].option
      });
    }

    // 该题为单选题
    if (this.data.questionInfo.type == "单选题") {
      this.setData({
        radioA: false,
        radioB: false,
        radioC: false,
        radioD: false
      });

      for (let i = 0; i < zuodaInfo.length; i++) {
        if (
          this.data.questionArrays[this.data.index].que_no ===
          zuodaInfo[i].que_no
        ) {
          if (zuodaInfo[i].selectOption === "A") {
            this.setData({
              radioA: true,
              radioB: false,
              radioC: false,
              radioD: false
            });
            break;
          } else if (zuodaInfo[i].selectOption === "B") {
            this.setData({
              radioA: false,
              radioB: true,
              radioC: false,
              radioD: false
            });
          } else if (zuodaInfo[i].selectOption === "C") {
            this.setData({
              radioA: false,
              radioB: false,
              radioC: true,
              radioD: false
            });
            break;
          } else if (zuodaInfo[i].selectOption === "D") {
            this.setData({
              radioA: false,
              radioB: false,
              radioC: false,
              radioD: true
            });
          }
        }
      }
    }
    //该题为多选题
    else if (this.data.questionInfo.type == "多选题") {
      this.setData({
        checkedboxA: false,
        checkedboxB: false,
        checkedboxC: false,
        checkedboxD: false
      });

      for (let i = 0; i < mulZuoDaInfo.length; i++) {
        if (
          this.data.questionArrays[this.data.index].que_no ===
          mulZuoDaInfo[i].que_no
        ) {
          let AIsSelected = mulZuoDaInfo[i].mulSelectOption.indexOf("A");
          let BIsSelected = mulZuoDaInfo[i].mulSelectOption.indexOf("B");
          let CIsSelected = mulZuoDaInfo[i].mulSelectOption.indexOf("C");
          let DIsSelected = mulZuoDaInfo[i].mulSelectOption.indexOf("D");

          if (AIsSelected != -1) {
            this.setData({
              checkedboxA: true
            });
          }

          if (BIsSelected != -1) {
            this.setData({
              checkedboxB: true
            });
          }

          if (CIsSelected != -1) {
            this.setData({
              checkedboxC: true
            });
          }

          if (DIsSelected != -1) {
            this.setData({
              checkedboxD: true
            });
          }
        }
      }
    } else {
      //主观题
      this.setData({
        textContext: ""
      });

      for (let i = 0; i < zhuguanZuoDaInfo.length; i++) {
        if (
          this.data.questionArrays[this.data.index].que_no ===
          zhuguanZuoDaInfo[i].que_no
        ) {
          this.setData({
            textContext: zhuguanZuoDaInfo[i].context
          });
        }
      }
    }
  },

  /**************************************************************************************** */

  checkboxChange(e) {
    this.setData({
      mulSelectOption: e.detail.value
    });
    this.mulAddZuoda();
  },

  /**
   * 添加多选题的答题记录
   */
  mulAddZuoda: function() {
    let mulZuoDa = {
      que_no: this.data.questionArrays[this.data.index].que_no,
      question: this.data.questionArrays[this.data.index].question,
      goodkey: this.data.questionArrays[this.data.index].goodkey,
      mulSelectOption: this.data.mulSelectOption
    };

    if (mulZuoDaInfo.length === 0) {
      mulZuoDaInfo.push(mulZuoDa);
    } else {
      let noInsert = true;
      for (let i = 0; i < mulZuoDaInfo.length; i++) {
        //找到已经答过的题，加以修改
        if (mulZuoDa.que_no === mulZuoDaInfo[i].que_no) {
          mulZuoDaInfo[i] = mulZuoDa;
          noInsert = false;
          break;
        }
      }
      //没有找到到已经答过的题，添加到答题数组中
      if (noInsert) {
        mulZuoDaInfo.push(mulZuoDa);
      }
    }
  },

  /******************************************************************************* */

  /**
   * 获得输入框中的内容
   */
  getTextAreaCon: function(event) {
    let zhuguanZuoDa = {
      que_no: this.data.questionArrays[this.data.index].que_no,
      question: this.data.questionArrays[this.data.index].question,
      context: event.detail.value
    };

    if (zhuguanZuoDaInfo.length === 0) {
      zhuguanZuoDaInfo.push(zhuguanZuoDa);
    } else {
      let noInsert = true;
      for (let i = 0; i < zhuguanZuoDaInfo.length; i++) {
        //找到已经答过的题，加以修改
        if (zhuguanZuoDa.que_no === zhuguanZuoDaInfo[i].que_no) {
          zhuguanZuoDaInfo[i] = zhuguanZuoDa;
          noInsert = false;
          break;
        }
      }
      //没有找到到已经答过的题，添加到答题数组中
      if (noInsert) {
        zhuguanZuoDaInfo.push(zhuguanZuoDa);
      }
    }
  },

  /******************************************************************************* */

  /**
   * 交卷
   */
  handIn: function() {
    let that = this;
    wx.showModal({
      content: "确定要交卷吗？",
      success(res) {
        if (res.confirm) {
          util
            .request(
              api.modify,
              {
                stu_no: app.globalData.no,
                profession_no: that.options.profession_no,
                course_no: that.options.course_no,
                zuodaInfo: zuodaInfo,
                mulZuoDaInfo: mulZuoDaInfo,
                zhuguanZuoDaInfo: zhuguanZuoDaInfo,
                mark1: that.options.mark1,
                mark2: that.options.mark2
              },
              "POST"
            )
            .then(function(res) {
              wx.showLoading({
                title: "数据正在提交......"
              });

              setTimeout(function() {
                if (res.data.data.code === 100) {
                  wx.hideLoading();
                  wx.reLaunch({
                    url: "../index/index"
                  });
                }
              }, 2000);
            });
        }
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    zuodaInfo = [];
    mulZuoDaInfo = [];
    zhuguanZuoDaInfo = [];
    this.setData({
      index: 0
    });

    //初始化第一题题目以及选项，题号从1开始
    var that = this;
    util
      .request(
        api.questions,
        {
          profession_no: this.options.profession_no,
          course_no: this.options.course_no,
          num1: this.options.num1,
          num2: this.options.num2,
          num3: this.options.num3
        },
        "POST"
      )
      .then(function(res) {
        let tempQuestionArrays = res.data.data.questions;
        let tempQuestionInfo = res.data.data.questions[0];
        let tempOptionInfo = res.data.data.questions[0].option;
        that.setData({
          questionArrays: tempQuestionArrays,
          questionInfo: tempQuestionInfo,
          optionInfo: tempOptionInfo
        });

        //倒计时

        var Interval = setInterval(function() {
          // 目标日期时间戳
          const end = Date.parse(new Date(that.options.etime));
          // 当前时间戳
          const now = Date.parse(new Date());
          // 相差的毫秒数
          const msec = end - now;
          // 计算时分秒数
          let hr = parseInt((msec / 1000 / 60 / 60) % 24);
          let min = parseInt((msec / 1000 / 60) % 60);
          let sec = parseInt((msec / 1000) % 60);
          // 个位数前补零
          hr = hr > 9 ? hr : "0" + hr;
          min = min > 9 ? min : "0" + min;
          sec = sec > 9 ? sec : "0" + sec;

          let tmpTime = hr + ":" + min + ":" + sec;
          if (hr == "00" && min == "00" && sec == "00") {
            clearInterval(Interval);

            //强制交卷
            util
              .request(
                api.modify,
                {
                  stu_no: app.globalData.no,
                  profession_no: that.options.profession_no,
                  course_no: that.options.course_no,
                  zuodaInfo: zuodaInfo,
                  mulZuoDaInfo: mulZuoDaInfo,
                  zhuguanZuoDaInfo: zhuguanZuoDaInfo,
                  mark1: that.options.mark1,
                  mark2: that.options.mark2
                },
                "POST"
              )
              .then(function(res) {
                wx.showLoading({
                  title: "考试结束......"
                });

                setTimeout(function() {
                  if (res.data.data.code === 100) {
                    wx.hideLoading();
                    wx.reLaunch({
                      url: "../index/index"
                    });
                  }
                }, 2000);
              });
          }
          that.setData({
            time: tmpTime
          });
        }, 1000);
      });
  },

  countdown: function() {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {}
});

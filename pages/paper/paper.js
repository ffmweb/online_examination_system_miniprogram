// pages/paper/paper.js

const api = require("../../utils/api.js");
const util = require("../../utils/util.js");

var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    paper: []
  },

  /**
   * 检查是否重复参加考试
   */
  check(event) {
    var that = this;

    if (event.currentTarget.dataset.statue === 2) {
      wx.showModal({
        content: "本场考试未开始，请安心备考。",
        showCancel: false
      });
    } else {
      util
        .request(
          api.againTestUrl,
          {
            profession_no:
              that.data.paper[event.currentTarget.dataset.index].profession_no,
            course_no:
              that.data.paper[event.currentTarget.dataset.index].course_no,
            student_no: app.globalData.no
          },
          "POST"
        )
        .then(function(res) {
          if (res.data.code === 101) {
            wx.showModal({
              content:
                "你已经参加过该场考试，不能重复参加。相关课程的成绩可在[我的成绩]中查询",
              showCancel: false
            });
          } else {
            wx.reLaunch({
              url:
                "../test/test?profession_no=" +
                that.data.paper[event.currentTarget.dataset.index]
                  .profession_no +
                "&course_no=" +
                that.data.paper[event.currentTarget.dataset.index].course_no +
                "&paper_no={{ item.paper_no }}" +
                that.data.paper[event.currentTarget.dataset.index].paper_no +
                "&num1=" +
                that.data.paper[event.currentTarget.dataset.index].num1 +
                "&mark1=" +
                that.data.paper[event.currentTarget.dataset.index].mark1 +
                "&num2=" +
                that.data.paper[event.currentTarget.dataset.index].num2 +
                "&mark2=" +
                that.data.paper[event.currentTarget.dataset.index].mark2 +
                "&num3=" +
                that.data.paper[event.currentTarget.dataset.index].num3 +
                "&stime=" +
                that.data.paper[event.currentTarget.dataset.index].stime +
                "&etime=" +
                that.data.paper[event.currentTarget.dataset.index].etime
            });
          }
        });
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    util
      .request(api.paper, { profession: app.globalData.profession }, "POST")
      .then(function(res) {
        that.setData({
          paper: res.data.data.paper
        });
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {}
});
